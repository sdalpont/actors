<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Prérequis

Pour démarer le projet il faut quelques prérequis : Docker, PHP8.3, NodeJS, Composer

## Installation
À la racine du projet exécuter

```
docker compose up -d 
```


Pour générer les librairies pour la partie front exécuter la commande :

```
npm install 
```
Pour générer les librairies pour la partie Back exécuter la commande :

```
composer install 
```

Ensuite il faut configurer le fichier .env du projet en se basant sur le code ci-dessous

```
APP_NAME=Laravel
APP_ENV=debug
APP_KEY=base64:x7lnuglMpcVPJLH4RhJ0vTkEwb78uX03dAxV5+HV3JE=
APP_DEBUG=true
APP_URL=http://127.0.0.1:8080

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=mysql

DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=db
DB_USERNAME=user
DB_PASSWORD=password

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DISK=local
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=mailpit
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS="hello@example.com"
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=
AWS_USE_PATH_STYLE_ENDPOINT=false

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_HOST=
PUSHER_PORT=443
PUSHER_SCHEME=https
PUSHER_APP_CLUSTER=mt1

VITE_APP_NAME="${APP_NAME}"
VITE_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
VITE_PUSHER_HOST="${PUSHER_HOST}"
VITE_PUSHER_PORT="${PUSHER_PORT}"
VITE_PUSHER_SCHEME="${PUSHER_SCHEME}"
VITE_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
```
Pour créer les tables dans la base de donnée exécuter la commande :
```
php artisan migrate
```

Pour remplir la base de donnée avec des données fictive vous pouvez lancer la commande :

```
php artisan  db:seed
```

Lancer a commande

```
php artisan serve --port=8080
```

Puis

```
npm run dev
```
Vous pouvez vous rendre sur l'URL indiquer dans le terminal de la commande ci-dessus soit http://127.0.0.1:8000

## Test

Pour lancer les tests du projet vous pouvez exécuter la commande :
```
php artisan test 
```
