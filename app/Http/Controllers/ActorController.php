<?php

namespace App\Http\Controllers;

use App\Http\Requests\Actor\StoreActorRequest;
use App\Http\Requests\Actor\UpdateActorRequest;
use App\Models\Actor;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ActorController extends Controller
{
    /*
     * Je n'ai pas implementer la structure desgin partern Repository/Service/Controller
     * Vu le peu de code metier et le peu d'interaction avec la base j'ai préféré utiliser eloquent directement
     */


    public function uploadFileForActor(Request $request): JsonResponse
    {
        $fileName = time().'.'.$request->file->getClientOriginalExtension();
        $request->file->move(public_path('upload'), $fileName);
        return response()->json(["path" =>  "public/upload/" . $fileName]);
    }
    public function actors(): JsonResponse
    {
        $actors = Actor::query()->orderBy('last_name')->get()->all();
        return response()->json($actors);
    }
    public function store(StoreActorRequest $request): JsonResponse
    {
        DB::beginTransaction();
        try {
            $actor = new Actor([
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
                'description' => $request->input('description'),
                'picture' => $request->input('picture')
            ]);
            $actor->save();
            DB::commit();
            return response()->json($actor->toArray());
        } catch (Exception $e) {
            DB::rollBack();
            report($e);
            return response()->json(['error' => 'An error occurred while storing actor!'],500);
        }
    }
    public function update($id, UpdateActorRequest $request): JsonResponse
    {
        /** @var Actor|null $actor */
        $actor = Actor::query()->find($id);
        if (isset($actor)) {
            DB::beginTransaction();
            try {
                $actor->update($request->all());
                DB::commit();
                return response()->json($actor);
            } catch (Exception $e) {
                DB::rollBack();
                report($e);
                return response()->json(['error' => 'An error occurred while updating actor!'], 500);
            }
        } else {
            return response()->json(['error' => 'No actor found'], 500);
        }
    }
    public function destroy($id): JsonResponse
    {
        /** @var Actor|null $actor */
        $actor = Actor::query()->find($id);
        if (isset($actor)) {
            DB::beginTransaction();
            try {
                $actor->delete();
                DB::commit();
                return response()->json([]);
            } catch (Exception $e) {
                DB::rollBack();
                report($e);
                return response()->json(['error' => 'An error occurred while deleting actor!'], 500);
            }
        } else {
            return response()->json(['error' => 'No actor found'], 500);
        }
    }
}
