<?php

namespace App\Http\Requests\Actor;

use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\ArrayShape;

class UpdateActorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        // Add logic to check if the user is authorized to submit this data.
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    #[ArrayShape(['last_name' => "string", 'first_name' => "string", 'description' => "string", 'picture' => 'string'])]
    public function rules(): array
    {
        return [
            'last_name' => 'required|string',
            'first_name' => 'required|string',
            'description' => 'string|nullable',
            'picture' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }

    /**

     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    #[ArrayShape(['last_name.required' => "string", 'first_name.required' => "string"])]
    public function messages(): array
    {
        return [
            'last_name.required' => 'Le nom de la célébrité doit être remplis',
            'first_name.required' => 'Le prénom de la célébrité doit être remplis',
        ];
    }
}
