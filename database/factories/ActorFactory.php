<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Actor>
 */
class ActorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    #[ArrayShape(['last_name' => "string", 'first_name' => "string", 'description' => "string", 'picture' => "string"])] public function definition(): array
    {
        return [
            'last_name' => fake()->name(),
            'first_name' => fake()->firstName(),
            'description' => fake()->paragraph(2),
            'picture' => null,
        ];
    }
}
