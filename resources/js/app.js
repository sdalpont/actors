import './bootstrap';
// Vuetify
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'

import App from './App.vue'
import {createApp} from 'vue'
import {store} from "../store/actors/store.ts";

const vuetify = createVuetify({
    components,
    directives
})

const app = createApp(App);
app.use(vuetify)
app.use(store)
app.mount("#app")

