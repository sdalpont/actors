export class Actor {
    public id?: number;
    public first_name: string;
    public last_name: string;
    public picture?: string;
    public description?: string;


    constructor() {
        this.last_name = ''
        this.first_name = ''
    }
}
