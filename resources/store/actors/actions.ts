import { ActionTree, ActionContext } from 'vuex'
import { State } from './state'
import { Mutations } from './mutations'
import { ActionTypes } from './types/action-types'
import { MutationTypes } from './types/mutation-types'
import axios, {AxiosResponse} from "axios";
import {Actor} from "../../model/Actor";

type AugmentedActionContext = {
    commit<K extends keyof Mutations>(
        key: K,
        payload?: Parameters<Mutations[K]>[1]
    ): ReturnType<Mutations[K]>
} & Omit<ActionContext<State, State>, 'commit'>

export interface Actions {
    [ActionTypes.SET_ACTORS](
        { commit }: AugmentedActionContext,
    ): Promise<void|AxiosResponse>
    [ActionTypes.DELETE_ACTOR](
        { commit }: AugmentedActionContext,
        payload: number
    ): Promise<void|AxiosResponse>
    [ActionTypes.UPDATE_ACTOR](
        { commit }: AugmentedActionContext,
        payload: Actor
    ): Promise<void|AxiosResponse>
    [ActionTypes.NEW_ACTOR](
        { commit }: AugmentedActionContext,
        payload: Actor
    ): Promise<void|AxiosResponse>
}

export const actions: ActionTree<State, State> & Actions = {
    [ActionTypes.SET_ACTORS]({ commit }) {
        commit(MutationTypes.SET_ACTORS_LOADING)
        return axios.get('/actor')
            .then((data) => {
                commit(MutationTypes.SET_ACTORS, data.data)
            })
    },
    [ActionTypes.DELETE_ACTOR]({ commit, dispatch }, id: number) {
        return axios.delete('/actor/' + id)
            .then(() => {
                commit(MutationTypes.SET_ACTORS_LOADING)
                dispatch(ActionTypes.SET_ACTORS)
            })
    },
    [ActionTypes.UPDATE_ACTOR]({ commit, dispatch }, payload) {
        return axios.put('/actor/' + payload.id, payload)
            .then(() => {
                commit(MutationTypes.SET_ACTORS_LOADING)
                dispatch(ActionTypes.SET_ACTORS)
            })
    },
    [ActionTypes.NEW_ACTOR]({ commit, dispatch }, payload) {
        return axios.post('/actor', payload)
            .then(() => {
                commit(MutationTypes.SET_ACTORS_LOADING)
                dispatch(ActionTypes.SET_ACTORS)
            })
    },
}
