import { GetterTree } from 'vuex'
import { State } from './state'
import {Actor} from "../../model/Actor";

export type Getters = {
    actors(state: State): Array<Actor>
    loading(state: State): Boolean
}

export const getters: GetterTree<State, State> & Getters = {
    actors: (state) => {
        return state.actors
    },
    loading: (state) => {
        return state.loading
    },
}
