import {MutationTypes} from './types/mutation-types'
import {State} from './state'
import {MutationTree} from "vuex";
import {Actor} from "../../model/Actor";

export type Mutations = {
    [MutationTypes.SET_ACTORS](state: State, payload: Array<Actor>): void
    [MutationTypes.UPDATE_ACTOR](state: State, payload: Actor): void
    [MutationTypes.DELETE_ACTOR](state: State, payload: number): void
    [MutationTypes.NEW_ACTOR](state: State, payload: Actor): void
    [MutationTypes.SET_ACTORS_LOADING](state: State): void
}

export const mutations: MutationTree<State> & Mutations = {
    [MutationTypes.SET_ACTORS](state, payload) {
        state.actors = payload
        setTimeout(() =>  state.loading = false, 500)
    },
    [MutationTypes.NEW_ACTOR](state, payload: Actor) {
        state.actors.push(payload)
        setTimeout(() =>  state.loading = false, 500)
    },
    [MutationTypes.UPDATE_ACTOR](state, payload: Actor) {
        state.actors = state.actors.filter((actor: Actor) => actor.id !== payload.id)
        state.actors.push(payload)
        setTimeout(() =>  state.loading = false, 500)
    },
    [MutationTypes.DELETE_ACTOR](state, payload: number) {
        state.actors = state.actors.filter((actor: Actor) => actor.id !== payload)
        setTimeout(() =>  state.loading = false, 500)
    },
    [MutationTypes.SET_ACTORS_LOADING](state) {
        setTimeout(() =>  state.loading = false, 500)
    },
}
