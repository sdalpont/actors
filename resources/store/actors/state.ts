import {Actor} from "../../model/Actor";

export const state : State = {
    actors: new Array<Actor>(),
    loading: false
}
export interface State {
    actors: Array<Actor>
    loading: Boolean
}
