export enum MutationTypes {
    SET_ACTORS = 'SET_ACTORS',
    DELETE_ACTOR = 'DELETE_ACTOR',
    UPDATE_ACTOR = 'UPDATE_ACTOR',
    NEW_ACTOR = 'NEW_ACTOR',
    SET_ACTORS_LOADING = 'SET_ACTORS_LOADING',
}
