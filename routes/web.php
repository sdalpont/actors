<?php

use App\Http\Controllers\ActorController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('app');
})->name('application');

Route::controller(ActorController::class)->group(function () {
    Route::prefix('actor')->group(function () {
        Route::get('/', 'actors');
        Route::post('/', 'store');
        Route::put('/{id}', 'update');
        Route::delete('/{id}', 'destroy');
        Route::post('/upload', 'uploadFileForActor');
    });
});

