<?php

namespace Tests\Feature\Actors;

use Tests\TestCase;

class ActorEndpointAPITest extends TestCase
{
    /**
     * A niveau des tests je n'ai pas fait de test unitaire car je n'ai pas de fonction a tester de manière unitaire
     */


    public function test_list_endpoint(): void
    {
        $response = $this->get('/api/actor');
        $response->assertStatus(200);
    }
}
