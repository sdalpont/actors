<?php

namespace Tests\Feature\Actors;

use App\Models\Actor;
use Tests\TestCase;

class ActorEndpointTest extends TestCase
{
    /**
     * A niveau des tests je n'ai pas fait de test unitaire car je n'ai pas de fonction a tester de manière unitaire
     */

    public function test_list_endpoint(): void
    {
        $response = $this->get('/actor');
        $response->assertStatus(200);
    }

    public function test_update_endpoint(): void
    {
        $data = [
            "first_name" => "zinzin",
            "last_name" => "zozo",
            "description" => "description",
        ];
        $response = $this->post('/actor/', $data);
        $response->assertStatus(200);
        $actorDB = Actor::query()->orderBy('id','DESC')->first();
        self::assertEquals($actorDB->first_name, $data["first_name"]);
        self::assertEquals($actorDB->last_name, $data["last_name"]);
    }

    public function test_new_endpoint(): void
    {
        $actor = Actor::factory(1)->create();
        $data = [
            "first_name" => "zinzin",
            "last_name" => "zozo",
        ];
        foreach ($actor as $at) {
            $response = $this->put('/actor/' . $at["id"], $data);
            $response->assertStatus(200);
            $actorDB = Actor::query()->find($at["id"]);
            self::assertEquals($actorDB->first_name, $data["first_name"]);
            self::assertEquals($actorDB->last_name, $data["last_name"]);
        }
    }

    public function test_delete_endpoint(): void
    {
        $actor = Actor::factory(1)->create();
        foreach ($actor as $at) {
            $response = $this->delete('/actor/' . $at["id"]);
            $response->assertStatus(200);
        }
    }
}
